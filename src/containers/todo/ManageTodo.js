import React, { useState } from "react";
import TodoLists from '../../components/TodoLists'
import AddTodo from '../../components/AddTodo'

export default function ManageTodo() {
  

  return (
    <div className="animate__animated animate__fadeIn"><hr/>
      <h2>Welcome to Manage Todos page.</h2> <hr/>
      <div className="container-fluid px-5">
        <div class="row">
          <div class="col-md-4">
              <div className="card card-body">                
                <h3 className="text-left">Add Todo</h3> <hr/>
                <AddTodo/>
              </div>
          </div>

          <div class="col-md-8 bg-white">
            <br/>            
            <h3>Todo Lists</h3><hr/>
            <TodoLists/>
          </div>
        </div>
      </div>

      <br/><hr/>
    </div>
  )
}
