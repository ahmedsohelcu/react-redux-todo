import React,{Fragment,useEffect,useState} from 'react'
import TodoLists from '../../components/TodoLists';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";

//for date format
import Moment from 'react-moment';
import 'moment-timezone';
import moment from 'moment'

import { useDispatch,useSelector } from "react-redux";

import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./../../../src/assets/css/bootstrap.css";
import { useHistory, useParams } from "react-router-dom";
import { getTodoByIdAction, updateTodoByIdAction } from '../../redux/actions/todos/TodoActions';

import Api from '../../apis/Api';
import { stubString } from 'lodash';

export default function EditTodo(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const params = useParams();

  const [inputs,setInput] = useState({
    name:'',
    description:'',
    dead_line:'',
  });
  const [dead_line, setDeadline] = useState('');


  useEffect(() => {
    // dispatch(getTodoByIdAction(params.id))
    updateState();
  }, [params.id]);

  const state = useSelector(state => state.getTodoById);
  const {loading, todo} = state;  
   
  
  //-------------------------------------------------
  //update state with current db value
  // ------------------------------------------------
  const updateState = async () =>{
    await Api().get(`/edit/todo/${params.id}`)
    .then(response=>{
      const {id,name,user_id,description,dead_line} = response.data.data;
      setInput({
        name:name,
        description:description,
        dead_line:dead_line,
      });
    }).catch(error=>{ 
      // console.log(error.response)
    })
  };

  //-------------------------------------------------
  //form data 
  // ------------------------------------------------
  const handleInputChange = (e)=>{    
    setInput({...inputs,
      [e.target.name]:e.target.value
    });
  }  
  
  //------------------------------------------------
  //date format and update state
  // ------------------------------------------------
  const changeDate = (date)=>{
    setInput({...inputs,
      dead_line: moment(date).format('L')
    });
  }

  const errors=null;
  const submitTodoForm = (e)=>{
    e.preventDefault();
    const {name,dead_line,description} = inputs;
    const id = params.id;
    dispatch(updateTodoByIdAction(id,name,dead_line,description));
  }
  
  const updateTodo= useSelector(state => state.updateTodoById)
  if(updateTodo.todo?.data.status === true){
    //console.log('success');
    history.push('/manage_todo');
    
    // setInput({...inputs,
    //   name:'',
    //   dead_line:'',
    //   description:''
    // });
  }


  return (
    <div className="animate__animated animate__fadeIn"><hr/>
      <h2>Welcome to Manage Todos page</h2> <hr/>
      <div className="container-fluid px-5">
        <div class="row">
          <div class="col-md-4 callout-subtitle">
              <div className="card card-body">                
                <h3 className="text-left">Edit Todo and id is </h3> <hr/>
               
                { loading?<p>Loading</p>: ''}

                <form onSubmit={submitTodoForm}>
                  {/* { error ? <p className="text-light">{error}</p> : '' }         */}
                  <fieldset>  
                    <div className="form-group">
                      <label className="form-label mt-4 float-left">Name</label>
                    
                      <input type="text" 
                          className="form-control"
                          name="name"
                          placeholder="Enter Name"
                          onChange={handleInputChange}                          
                          value={inputs.name }
                        />              
                        <div className="validation_error_message">
                          { errors&&errors.name?<p>{errors.name[0]}</p>: ''}              
                        </div>
                    </div>

                    <div className="form-group">
                      <label className="form-label mt-4 float-left">Deadline</label>
                      {/* <DatePicker className="form-control" selected={dead_line} value={todo?todo.dead_line: null} onChange={(date) => setDeadline(date)} /> */}
                      <DatePicker className="form-control" selected={dead_line} value={inputs.dead_line} onChange={(date) => changeDate(date)
                      } />

                      <div className="validation_error_message">
                        { errors&&errors.dead_line?<p>{errors.dead_line[0]}</p>: ''}   
                      </div>
                    </div>

                    <div className="form-group">
                      <label className="form-label mt-4 float-left">Details</label>
                      <textarea 
                          rows="5"
                          className="form-control"
                          placeholder="Enter details"
                          name="description"
                          value={inputs.description}
                          onChange={handleInputChange}
                          >{handleInputChange}
                        </textarea>  
                        <div className="validation_error_message">{ errors&&errors.description?<p>{errors.description[0]}</p>: ''}</div>
                    </div>                 
                    
                    <br/>
                    <div className="form-group">
                      <button type="submit" className="form-control btn btn-primary float-right">Update</button>            
                    </div>
                  </fieldset>    
                </form>
                

              </div>
          </div>

          <div class="col-md-8 bg-white">
            <br/>            
            <h3>Todo Lists</h3><hr/>
            <TodoLists/>
          </div>
        </div>
      </div>

      <br/><hr/>
    </div>
  )
}
