import React from 'react'
import { Fragment } from 'react'
import './registration.css'
import { useState,useEffect } from 'react'
import { registerUserAction } from '../../redux/actions/todos/UserActions';
import { useDispatch, useSelector } from 'react-redux';
import '../../App.css'
import { useHistory } from 'react-router-dom';


export default function Registration() {
  const dispatch = useDispatch();
  const history = useHistory();
  //-------------------------------------------
  //initialize state
  //-------------------------------------------
  const initialState = {
    name:'',
    username:'',
    email:'',
    password:'',
    password_confirmation:'',
    picture:'',
    details:'',
  }
  const [inputs,setInput] = useState(initialState);  
  
  //-------------------------------------------
  //reset form
  //-------------------------------------------
  const resetRegForm = ()=>{
    setInput(initialState);
  }
  
  //-------------------------------------------
  //change input
  //-------------------------------------------
  const onChangeHandler = (e)=>{
    setInput({
      ...inputs,
      [e.target.name]:e.target.value
    });
  }
  
  //-------------------------------------------
  //dispatch action for registration 
  //-------------------------------------------
  const register =(e)=>{
    e.preventDefault();
    const {name,username,email,password,password_confirmation,picture,details} = inputs; 
    dispatch(registerUserAction(name,username,email,password,password_confirmation,picture,details)); 
  }

  //-------------------------------------------
  //set error message 
  //-------------------------------------------
  const {status,message,errors} = useSelector(state => state.userRegistration);
  
  useEffect(() => {
  
  }, []);

  return (
    <Fragment>
      <hr/>
      <h2>Registration section..</h2>
        <p>Loading....</p> :(
          <div className="container card card-body animate__animated animate__fadeIn">        
         
          <form onSubmit={register}>
            <div class="row">
              <div class="col-md-7">            
              {status?status:''}
              {message?message:''}
                <fieldset>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1" className="form-label mt-4 float-left">Name</label>
                    <input type="text" 
                        className="form-control" 
                        placeholder="Enter name" 
                        name="name"
                        value={inputs.name}
                        onChange={onChangeHandler}
                    />
                    { errors ? <p className="errMsg">{errors && errors.name?.[0]}</p> : '' }
                  </div>

                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1" className="form-label mt-4 float-left">Username</label>
                    <input type="text" 
                        className="form-control" 
                        placeholder="Enter Username" 
                        name="username" 
                        value={inputs.username}
                        onChange={onChangeHandler}
                    />
                    { errors ? <p className="errMsg">{errors && errors.username?.[0]}</p> : '' }
                  </div>
  
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1" className="form-label mt-4 float-left">Email address</label>
                    <input type="email" 
                        className="form-control" 
                        placeholder="Enter email" 
                        name="email" 
                        value={inputs.email}
                        onChange={onChangeHandler}
                    /> 
                    { errors ? <p className="errMsg">{errors && errors.email?.[0]}</p> : '' }
                  </div>
  
                  <div className="form-group">
                    <label className="form-label mt-4">Password</label>
                    <input type="password" 
                        name="password" 
                        className="form-control" 
                        placeholder="Enter password" 
                        value={inputs.password}
                        onChange={onChangeHandler}
                    />
                    { errors ? <p className="errMsg">{errors && errors.password?.[0]}</p> : '' }
                  </div>                 
                </fieldset>          
              </div>
  
              <div class="col-md-5">            
                <fieldset>     
                <div className="form-group">
                    <label className="form-label mt-4">Confirm Password</label>
                    <input type="password" 
                        name="password_confirmation" 
                        className="form-control" 
                        placeholder="Enter confirm password" 
                        value={inputs.password_confirmation}
                        onChange={onChangeHandler}
                    />    
                  </div>
                  { errors ? <p className="errMsg">{errors && errors.password_confirmation?.[0]}</p> : '' }
                             
                  <div className="form-group">
                    <label htmlFor="formFile" className="form-label mt-4">Profile Picture</label>
                    <input className="form-control" type="file" name="picture" id="formFile" />                  

                    { errors ? <p className="errMsg">{errors && errors.picture?.[0]}</p> : '' }
                  </div>
  
                  <div className="form-group">
                    <label htmlFor="exampleTextarea" className="form-label mt-4 align-left">Bio</label>
                    <textarea className="form-control" value={inputs.details} name="details"
                       onChange={onChangeHandler} rows={3}>
                      {inputs.details}
                    </textarea>
                    { errors ? <p className="errMsg">{errors && errors.details?.[0]}</p> : '' }
                  </div>
                </fieldset>         
                <br/>
                <div className="buttonWrapper">
                    <button type="button" onClick={resetRegForm} className="form-control btn btn-danger float-right">Reset</button>                                   
                    <button type="submit" className="form-control btn btn-success float-right">Submit</button>                                       
                </div>
              </div>                                
          </div>
          </form>
          <br/><br/>
        </div>    
        ) } 
     
      <br/>
    </Fragment>
  )
}






