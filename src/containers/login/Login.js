import React,{Fragment} from 'react'
import { useState,useEffect } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { loginUserAction } from '../../redux/actions/todos/UserActions';

export default function Login(){
  const dispatch = useDispatch();
  const [inputs,setInput] = useState({
    email:'',
    password:'',
  });

  const inputChangeHandler = (e)=>{
    setInput({
      ...inputs,
      [e.target.name]:e.target.value
    });
  }

  const submitLoginForm = (e)=>{
    e.preventDefault();
    const {email,password} = inputs;
    dispatch(loginUserAction(email,password));
  }

 
  const {loading, errors} = useSelector(state => state.loginInfo);





  // setInput({
  //   email:'',
  //   password:'',
  // });
  

  return (
    <Fragment>
      <hr/>
      <h2>Login</h2>
      <div className="container col-md-4">
        <form onSubmit={submitLoginForm}>
          <div class="row mx-auto">
            <div class="col-md-5 card card-body callout-subtitle animate__animated animate__fadeIn">            
              <fieldset>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1" className="form-label mt-4 float-left">Email address</label>
                  <input type="email" 
                    name="email" className="form-control" 
                    value={inputs.email}
                    onChange={inputChangeHandler} 
                    placeholder="Enter email" 
                  />        
                   { errors ? <p className="errMsg">{errors && errors.email }</p> : '' }
                </div>
                
                <div className="form-group">
                  <label className="form-label mt-4">Password</label>
                  <input type="password"
                    name="password"
                    className="form-control"
                    value={inputs.password} 
                    onChange={inputChangeHandler} 
                    placeholder="Password"
                  />                  
                  { errors ? <p className="errMsg">{errors && errors.password }</p> : '' }
                </div>                         
                
                <br/>
                <div className="form-group">
                  <button type="submit" className="form-control btn btn-primary float-right">Login</button>
                </div>
              </fieldset>                        
           </div> 

           <div class="offset-md-4"></div>       
        </div>       
        </form>
      </div>    
    </Fragment>
  )
  
}
