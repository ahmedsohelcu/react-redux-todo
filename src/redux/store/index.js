import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createTodoReducer } from '../reducers/todos/CreateTodoReducer';
import { todolistsReducer } from '../reducers/todos/TodoListsReducer';
import { deleteTodoReducer } from '../reducers/todos/DeleteTodoReducer';
import { getTodoByIdReducer, updateTodoByIdReducer } from '../reducers/todos/EditTodoReducer';
import { loginUserReducer, registerUserReducer } from '../reducers/todos/UserReducers';

const middlewares = [thunk];

const reducer = combineReducers({
  createTodo: createTodoReducer,
  getAllTodos:todolistsReducer,  
  deleteTodoById:deleteTodoReducer,
  updateTodoById:updateTodoByIdReducer,  
  getTodoById:getTodoByIdReducer,
  userRegistration:registerUserReducer,
  loginInfo:loginUserReducer,
    
});

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;

// export {store};
