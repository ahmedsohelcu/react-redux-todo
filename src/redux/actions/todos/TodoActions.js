
import {
  CREATE_TODO_REQUEST,
  CREATE_TODO_SUCCESS,
  CREATE_TODO_ERROR,

  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  FETCH_TODO_ERROR,

  DELETE_TODO_REQUEST,
  DELETE_TODO_SUCCESS,
  DELETE_TODO_ERROR,
  
  GET_TODO_BY_ID_REQUEST,
  GET_TODO_BY_ID_SUCCESS,
  GET_TODO_BY_ID_ERROR,

  UPDATE_TODO_BY_ID_REQUEST,
  UPDATE_TODO_BY_ID_SUCCESS,
  UPDATE_TODO_BY_ID_ERROR,
} from '../../types/index'

import Api from '../../../apis/Api'

const createTodoAction = (name,description,dead_line)=>{
  return async dispatch =>{
    try {
      dispatch({
        type: 'CREATE_TODO_REQUEST'
      });
  
      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
  
      const {data} = await Api().post('/user_store_todo',{name,description,dead_line});

      dispatch({
        type: 'CREATE_TODO_SUCCESS',
        payload:data,
      }); 

      //fetch data after create new one
     dispatch(fetchTodoListsAction());

    } catch (error) {
      dispatch({
        type: 'CREATE_TODO_ERROR',        
        payload:error.response && error.response.data.errors,
      });
    }

  }
}

export {createTodoAction};


//--------------------------------------------
//FETCH TODO
//--------------------------------------------
export const fetchTodoListsAction = ()=>{
  return async dispatch =>{
    try {
      dispatch({
        type: 'FETCH_TODO_REQUEST'
      });
  
      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
  
      const {data} = await Api().get('/user_todos',config);

      dispatch({
        type: 'FETCH_TODO_SUCCESS',
        payload:data,
      });
      
    } catch (error) {
      dispatch({
        type: 'FETCH_TODO_ERROR',
        payload:error.response && error.response.data.errors,
      });
    }

  }
}


//--------------------------------------------
//DELETE TODO
//--------------------------------------------
export const deleteTodoListAction = (id)=>{
  return async dispatch =>{
    try {
      dispatch({
        type: 'DELETE_TODO_REQUEST'
      });
  
      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
  
      const {data} = await Api().post(`/user_delete_todo/${id}`,config);

      dispatch({
        type: 'DELETE_TODO_SUCCESS',
        payload:data,
      });

      //fetch al todos after delete one
      dispatch(fetchTodoListsAction());
      
    } catch (error) {
      dispatch({
        type: 'DELETE_TODO_ERROR',
        payload:error.response && error.response.data.errors,
      });
    }

  }
}


//--------------------------------------------
//EDIT TODO
//--------------------------------------------
export const getTodoByIdAction=(id)=>{
  return async dispatch =>{
    try {
      dispatch({
        type: 'GET_TODO_BY_ID_REQUEST'
      });
  
      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
  
      const {data} = await Api().get(`/edit/todo/${id}`);
      dispatch({
        type: 'GET_TODO_BY_ID_SUCCESS',
        payload:data,
      }); 
  
    } catch (error) {
      dispatch({
        type: 'GET_TODO_BY_ID_ERROR',        
        payload:error.response && error.response.data.errors,
      });
    }
  }  
}

//--------------------------------------------
//EDIT TODO
//--------------------------------------------
export const updateTodoByIdAction=(id,name,dead_line,description)=>{
  return async dispatch =>{
    try {
      dispatch({
        type: 'UPDATE_TODO_BY_ID_REQUEST'
      });
  
      const config = {
        headers:{
          'Content-Type': 'application/json',
        }
      };  
      const data = await Api().post('/user_todo/update/',{id,name,dead_line,description});
      
      dispatch({
        type: 'UPDATE_TODO_BY_ID_SUCCESS',
        payload:data,
      }); 

      //fetch al todos after delete one
      dispatch(fetchTodoListsAction());
  
    } catch (error) {
      dispatch({
        type: 'UPDATE_TODO_BY_ID_ERROR',        
        payload:error.response && error.response.data.errors,
      });
    }
  }  
}
