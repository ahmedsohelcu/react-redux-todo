import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,

  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,

} from '../../types';

import Api from '../../../apis/Api';


export const registerUserAction = (name,username,email,password,password_confirmation,picture,details)=>{
  return async dispatch =>{
    try {
      dispatch({
        type:'REGISTER_USER_REQUEST'
      });

      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
      
      const {data} = await Api().post('/user_registration',{name,username,email,password,password_confirmation,picture,details});

      dispatch({
        type:'REGISTER_USER_SUCCESS',
        payload:data,
      });

    } catch (error) {
      dispatch({
        type:'REGISTER_USER_ERROR',
        // payload: error.response && error.response.data.errors,
        payload: error.response && error.response.data?.errors,
      });
    }
  }
}

//-----------------------------------------------------------
//Login User Action
//-----------------------------------------------------------
export const loginUserAction = (email,password)=>{
  return async dispatch =>{
    try {
      dispatch({
        type:'LOGIN_USER_REQUEST'
      });

      const config = {
        headers:{
          'Content-Type': 'application/json'
        }
      };
      
      const {data} = await Api().post('/user_login',{email,password});

      dispatch({
        type:'LOGIN_USER_SUCCESS',
        payload:data,
      });

    } catch (error) {
      dispatch({
        type:'LOGIN_USER_ERROR',      
        payload: error.response && error.response.data?.errors,
      });
    }
  }
}








