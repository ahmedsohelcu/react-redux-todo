
import {
  CREATE_TODO_REQUEST,
  CREATE_TODO_SUCCESS,
  CREATE_TODO_ERROR,
  
  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  FETCH_TODO_ERROR
} from '../../types/index'


//--------------------------------------------
//CREATE TODO Reducer
//--------------------------------------------
const createTodoReducer = (initalState={},action)=>{
  switch(action.type){
    case CREATE_TODO_REQUEST:
      return{
        loading:true,
      };

    case CREATE_TODO_SUCCESS:
      return{
        loading:false,
        message:action.payload.message,
        status:action.payload.status,
      };

    case CREATE_TODO_ERROR:
      return{
        loading:false,
        errors:action.payload,
      };

    default:
      return initalState;
  }
}

export {createTodoReducer};


