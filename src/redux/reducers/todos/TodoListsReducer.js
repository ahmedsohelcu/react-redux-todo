
import {  
  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  FETCH_TODO_ERROR
} from '../../types/index'


//--------------------------------------------
//CREATE TODO Reducer
//--------------------------------------------
const todolistsReducer = (state=[],action)=>{
  switch(action.type){
    case FETCH_TODO_REQUEST:
      return{
        loading:true,
      };

    case FETCH_TODO_SUCCESS:
      return{
        loading:false,
        todos:action.payload.todos,
        status:action.payload.status,
      };

    case FETCH_TODO_ERROR:
      return{
        loading:false,
        errors:action.payload,
      };

    default:
      return state;
  }
}

export {todolistsReducer};


