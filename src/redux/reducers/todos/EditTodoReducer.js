import React, {Fragment} from 'react'

import {  
  GET_TODO_BY_ID_REQUEST,
  GET_TODO_BY_ID_SUCCESS,
  GET_TODO_BY_ID_ERROR,
  
  UPDATE_TODO_BY_ID_REQUEST,
  UPDATE_TODO_BY_ID_SUCCESS,
  UPDATE_TODO_BY_ID_ERROR,
} from '../../types/index'


//--------------------------------------------
//Edit todo Reducer
//--------------------------------------------
export const getTodoByIdReducer = (state={},action)=>{
  switch(action.type){
    case GET_TODO_BY_ID_REQUEST:
      return{
        loading:true,
      };

    case GET_TODO_BY_ID_SUCCESS:
      return{
        loading:false,
        todo:action.payload.data,  
      };

    case GET_TODO_BY_ID_ERROR:
      return{
        loading:false,
        errors:action.payload.data,
      };

    default:
      return state;
  }
};



//--------------------------------------------
//update todo Reducer
//--------------------------------------------
export const updateTodoByIdReducer = (state={},action)=>{
  switch(action.type){
    case UPDATE_TODO_BY_ID_REQUEST:
      return{
        loading:true,
      };

    case UPDATE_TODO_BY_ID_SUCCESS:
      return{
        loading:false,
        todo:action.payload        
      };

    case UPDATE_TODO_BY_ID_ERROR:
      return{
        loading:false,
        errors:action.payload,
      };

    default:
      return state;
  }
};


