import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR

} from '../../types';

import Api from '../../../apis/Api';

//--------------------------------------------
//REGISTER USER Reducer
//--------------------------------------------
export const registerUserReducer = (initialState={},action)=>{
  switch(action.type){
    case REGISTER_USER_REQUEST:
      return{
        loading:true,
      };

    case REGISTER_USER_SUCCESS:
      return{
        loading:false,
        message:action.payload?.message,
        status:action.payload?.status,
      };

    case REGISTER_USER_ERROR:
      return{
        loading:false,
        errors:action.payload,
        // error:{
        //   name: action.payload?.name?.[0],
        //   email: action.payload?.email?.[0],
        //   username: action.payload?.username?.[0],
        //   password: action.payload?.password?.[0],
        //   password_confirmation: action.payload?.password_confirmation?.[0],
        // }
      };

    default:
      return initialState;
  }
}

//--------------------------------------------
//LOGIN USER Reducer
//--------------------------------------------
export const loginUserReducer = (initialState={},action)=>{
  switch(action.type){
    case LOGIN_USER_REQUEST:
      return{
        loading:true,
      };

    case LOGIN_USER_SUCCESS:
      return{
        loading:false,
        message:action.payload?.message,
        status:action.payload?.status,
      };

    case LOGIN_USER_ERROR:
      return{
        loading:false,
        // errors:action.payload,
        errors:{
          email: action.payload?.email?.[0],
          password: action.payload?.password?.[0],
        }
      };

    default:
      return initialState;
  }
}
