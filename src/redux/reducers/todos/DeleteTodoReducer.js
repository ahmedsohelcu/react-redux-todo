
import {
  DELETE_TODO_REQUEST,
  DELETE_TODO_SUCCESS,
  DELETE_TODO_ERROR,
  
} from '../../types/index'


//--------------------------------------------
//CREATE TODO Reducer
//--------------------------------------------
const deleteTodoReducer = (id=null,action)=>{
  switch(action.type){
    case DELETE_TODO_REQUEST:
      return{
        loading:true,
      };

    case DELETE_TODO_SUCCESS:
      return{
        loading:false,
        message:action.payload.message,
        status:action.payload.status,
      };

    case DELETE_TODO_ERROR:
      return{
        loading:false,
        errors:action.payload,
      };

    default:
      return id;
  }
}

export {deleteTodoReducer};


