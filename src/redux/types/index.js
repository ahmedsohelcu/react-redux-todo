//------------------------------------------------------
//Create Todo by user 
//------------------------------------------------------
export const CREATE_TODO_REQUEST = 'CREATE_TODO_REQUEST';
export const CREATE_TODO_SUCCESS = 'CREATE_TODO_SUCCESS';
export const CREATE_TODO_ERROR = 'CREATE_TODO_ERROR';

//------------------------------------------------------
//FETCH_TODO
//------------------------------------------------------
export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR   = 'FETCH_TODO_ERROR';

//------------------------------------------------------
//DELETE_TODO
//------------------------------------------------------
export const DELETE_TODO_REQUEST = 'DELETE_TODO_REQUEST';
export const DELETE_TODO_SUCCESS = 'DELETE_TODO_SUCCESS';
export const DELETE_TODO_ERROR   = 'DELETE_TODO_ERROR';


//------------------------------------------------------
//EDIT TODO
//------------------------------------------------------
export const GET_TODO_BY_ID_REQUEST = 'GET_TODO_BY_ID_REQUEST';
export const GET_TODO_BY_ID_SUCCESS = 'GET_TODO_BY_ID_SUCCESS';
export const GET_TODO_BY_ID_ERROR   = 'GET_TODO_BY_ID_ERROR';


//------------------------------------------------------
//UPDATE TODO
//------------------------------------------------------
export const UPDATE_TODO_BY_ID_REQUEST = 'UPDATE_TODO_BY_ID_REQUEST';
export const UPDATE_TODO_BY_ID_SUCCESS = 'UPDATE_TODO_BY_ID_SUCCESS';
export const UPDATE_TODO_BY_ID_ERROR   = 'UPDATE_TODO_BY_ID_ERROR';

//------------------------------------------------------
//Create/Register User
//------------------------------------------------------
export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_ERROR   = 'REGISTER_USER_ERROR';


//------------------------------------------------------
//login User
//------------------------------------------------------
export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR   = 'LOGIN_USER_ERROR';