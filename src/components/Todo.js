import React,{Fragment,useEffect,useState} from 'react'
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'

//for date format
import Moment from 'react-moment';
import 'moment-timezone';

import EditTodo from '../containers/todo/EditTodo';
import {Link} from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { useDispatch,useSelector } from "react-redux";
import { deleteTodoListAction } from '../redux/actions/todos/TodoActions';

export default function Todo(props) {
  const history = useHistory();

  //------------------------------------------------
  //response //fetching data..
  //------------------------------------------------
 const dispatch = useDispatch();
 const state = useSelector(state => state.deleteTodoById);
//  const {loading , message ,status} = state;

  useEffect(() => {
    console.log('props');
  }, []);

  const deletTodo = (e)=>{    
    e.preventDefault();    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        //delete from db
        dispatch(deleteTodoListAction(e.target.value));
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }else{
        console.log('not fired');
      }
    });
  };

  const editTodo = (todo)=>{
    // console.log(todo);

    history.push({
      pathname: [`/edit_todo/${todo.id}`],
      state:todo
    });
  }

  
  return (
    <Fragment>
       {/* {message ? (<p>{message}</p>) : null } */}
      {
        props.todo ? (
          <tr>
            <td>{props.todo.id}</td>
            <td>{props.todo.name}</td>
            <td>{props.todo.description}</td>
            <td>              
                {props.todo.dead_line}            
            </td>            
            <td>
              <button type="button" className="btn btn-sm btn-info" value={props.todo.id} onClick={()=>editTodo(props.todo) }>
                Edit
              </button>
              <button type="button" className="btn-sm btn btn-danger" value={props.todo.id} onClick={deletTodo}>Delete</button>
            </td>
          </tr>
        ) : ''
      }
    </Fragment>
  )
}
