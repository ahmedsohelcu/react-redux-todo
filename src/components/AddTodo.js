import React, {Fragment,useState,useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import '../App.css'
import { useHistory } from "react-router-dom";
import { useDispatch,useSelector } from "react-redux";

import Api from '../apis/Api'
import {connect } from 'react-redux'
import { createTodoAction, fetchTodoListsAction } from "../redux/actions/todos/TodoActions";
// import {reset} from 'redux-form'

function AddTodo(props) {
  const history = useHistory();
  //-----------------------------------------------
  //first way
  //-----------------------------------------------
  // const [msg, setMsg] = useState({});

 //------------------------------------------------
 //Initial error message
 //------------------------------------------------
  const [error,setError] = useState('');
    
  //------------------------------------------------
  //form data 
  //------------------------------------------------
  const [name,setName] = useState('');
  const [description,setDescription] = useState('');
  const [dead_line, setDeadline] = useState(new Date());

 //------------------------------------------------
 //response
 //------------------------------------------------
  const dispatch = useDispatch();
  const state = useSelector(state=>state.createTodo);
  //when success
  const {loading,message,status,errors} = state;
  
  //-------------------------------------------
  //validation error
  //-------------------------------------------
  if(errors){
    const {name,description,dead_line} = errors;    
  }
  
  const submitTodoForm = (e)=>{
    e.preventDefault();
    //clear the previous error message before submit forms
    setError('');

    //------------------------------------------------------
    //Insert data
    //------------------------------------------------------
    dispatch(createTodoAction(name,description,dead_line));
    // dispatch(createTodoAction(null));
    
    }
  // };

  
  //-----------------------------------------------
  //UseEffect
  //-----------------------------------------------    
  useEffect((e)=>{

  },[]);



  return (
    <Fragment>
      
       { loading?<p>Loading</p>: ''}

      <form onSubmit={submitTodoForm} name="myForm">
        { error ? <p className="text-light">{error}</p> : '' }        
        <fieldset>  
          <div className="form-group">
            <label className="form-label mt-4 float-left">Name</label>
           
            <input type="text" 
                className="form-control"                 
                name="name" 
                placeholder="Enter Name"
                // onChange={handleInputChange}
                onChange={e=>setName(e.target.value)}
                value={name}
              />              
              <div className="validation_error_message">
                { errors&&errors.name?<p>{errors.name[0]}</p>: ''}              
              </div>
          </div>

          <div className="form-group">
            <label className="form-label mt-4 float-left">Deadline</label>
            <DatePicker className="form-control" selected={dead_line} onChange={(date) => setDeadline(date)} />
            <div className="validation_error_message">
              { errors&&errors.dead_line?<p>{errors.dead_line[0]}</p>: ''}   
            </div>

          </div>

          <div className="form-group">
            <label className="form-label mt-4 float-left">Details</label>
            <textarea 
                rows="5"
                className="form-control"
                placeholder="Enter details"
                name="description"
                value={description}
                // onChange={handleInputChange}                
                onChange={e=>setDescription(e.target.value)}
                >
              </textarea>  
              <div className="validation_error_message">{ errors&&errors.description?<p>{errors.description[0]}</p>: ''}</div>
          </div>                 
          
          <br/>
          <div className="form-group">
            <button type="submit" className="form-control btn btn-primary float-right">Add Todo</button>            
          </div>
        </fieldset>    
      </form>
    </Fragment>
  )
}


export default AddTodo;

// const mapStateToProps = (state)=>{
//   return {
//      books:'state.booksData.books || []',
//      // error: state.booksData.error || null,
//      // isLoading:state.booksData.isLoading,
//    } 
//  }
 
//  const mapDispatchToProps = (dispatch)=>({
//   /// addTodo : (data)=>dispatch(createTodoAction(data))
   
//  })
 
//  export default connect(mapStateToProps,mapDispatchToProps)(AddTodo);
