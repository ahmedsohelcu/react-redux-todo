import React,{Fragment,useEffect,useState} from 'react'
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import { useHistory } from "react-router-dom";

import { useDispatch,useSelector } from "react-redux";

import Todo from './Todo';
import { fetchTodoListsAction } from '../redux/actions/todos/TodoActions';

export default function TodoLists() {
  const history = useHistory();
 
 //------------------------------------------------
 //response //fetching data..
 //------------------------------------------------
 const dispatch = useDispatch();
 const state = useSelector(state => state.getAllTodos);
 const {loading,todos,status} = state;
//  console.log('-------------------');
//  console.log(loading);
//  console.log(todos);
//  console.log(status);
//  console.log('-------------------');

 //-----------------------------------------------
  //UseEffect
  //-----------------------------------------------    
  useEffect((e)=>{
    dispatch(fetchTodoListsAction())
   },[]);

  return (    
    <Fragment>
      <table className="table table-hover table-striped">              
          <thead>
            <tr>
              <th width="10%">SL</th>
              <th width="20%">Name</th>
              <th width="30%">Details</th>
              <th width="20%">Date</th>
              <th width="20%">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
                loading ? <p>Loading....</p> 
                : 
              (
                todos ? todos.map(todo=> <Todo key={todo.id} todo={todo} />) : ''
              )
              
            }  
          </tbody>      
        </table>

        {/* START TODOS PAGINATION */}
        <div className="todos_pagination">
          <ul className="pagination">
            <li className="page-item disabled">
              <a className="page-link" href="#">«</a>
            </li>
            <li className="page-item active">
              <a className="page-link" href="#">1</a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">2</a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">3</a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">4</a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">5</a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">»</a>
            </li>
          </ul>
        </div>
            {/* END PAGINATION */}
    </Fragment>
  )
}
