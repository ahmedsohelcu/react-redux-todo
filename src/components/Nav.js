import React, {Component,Fragment,useEffect,useState} from 'react';
import {Link} from 'react-router-dom';
//import './Nav.css'
// import Api from '../apis/Api'
// import { useHistory } from "react-router-dom";

function Nav(){
  //const [isLoggedIn, setIsLoggedIn] = useState('false');
  //const history = useHistory();

  // const logout = (e)=>{
  //   e.preventDefault();

  //   Api().post('/user_logout')
  //   .then(response=>{      
  //     if(response.data.status){
  //       localStorage.removeItem('token');
  //       localStorage.removeItem('loggedIn');

  //       history.push("/user_login");
  //       setIsLoggedIn('false');
  //     }
  //   })
  //   .catch(error=>{
  //     console.log(error.response);
  //   });
  // }

  //---------------------------------------
  //UserEffect
  // //------------------------------------
  useEffect(()=>{
    // //-------------------------------------
    // //has token or not 
    // //-------------------------------------
    // let token = localStorage.getItem("loggedIn");
    // if(token){
    //   setIsLoggedIn('true');
    // }else{
    //   setIsLoggedIn('false');
    // }
    //-----------------------------------
  });

  return (
      <div className="navItemWrapper">   
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
            </button>
            
            <div className="collapse navbar-collapse" id="navbarColor02">
              <ul className="navbar-nav me-auto">            
                <Link className="btn btn-success nav-item nav-link active" to="/">
                  Home
                </Link>
                
                <Link className="btn btn-success nav-item nav-link" to="/registration">
                  Registration
                </Link>
                
                <Link className="btn btn-success nav-item nav-link" to="/login">
                  Login
                </Link>

                <Link className="btn btn-success nav-item" to="/manage_todo">
                  To do
                </Link>

                {/* <Link className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                  <div className="dropdown-menu">
                    <a className="dropdown-item" href="#">Action</a>
                    <a className="dropdown-item" href="#">Another action</a>
                    <a className="dropdown-item" href="#">Something else here</a>
                    <div className="dropdown-divider" />
                    <a className="dropdown-item" href="#">Separated link</a>
                  </div>
                </Link> */}
              </ul>

              {/* <form className="d-flex">
                <input className="form-control me-sm-2" type="text" placeholder="Search" />
                <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
              </form> */}

            </div>
          </div>
        </nav>   
      </div>

      //   <Link className="navItem btn btn-success" to="/">Home</Link>
      //   <Link className="navItem btn btn-success" to="/user_create_todo">Create Todo</Link>
      //   <Link className="navItem btn btn-success" to="/user_todos">User Todos</Link>
      //   {/* <Link className="navItem btn btn-success" onClick={logout}>Logout</Link> */}
      //   <Link className="navItem btn btn-success">Logout</Link>
      //   <Link className="navItem btn btn-success" to="/user_registration">User Registration</Link>
      //   <Link className="navItem btn btn-success" to="/user_login">User Login</Link>
      // </div>


    );
}

export default Nav;