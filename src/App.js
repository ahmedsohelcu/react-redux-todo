import './App.css';
import logo from './logo.svg';
import React, { Component }  from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import "animate.css"

import {connect } from 'react-redux'


import Nav from './components/Nav';
import ManageTodo from './containers/todo/ManageTodo'
import Home from './containers/home/Home'
import Registration from './containers/registration/Registration'
import Login from './containers/login/Login'
import EditTodo from './containers/todo/EditTodo'

function App() {
  return (
    <Router>
      <div className="App">
          <Nav/>          
            <div className="minHeight content_wrapper">
              <Switch>
                <Route exact path="/">
                  <Home/>
                </Route>

                <Route exact path="/manage_todo">
                  <ManageTodo />
                </Route>

                <Route exact path="/registration">
                  <Registration />
                </Route>
                
                <Route exact path="/login">
                  <Login />
                </Route>

                <Route exact path="/edit_todo/:id">
                  <EditTodo />
                </Route>

                {/* <Route exact path="/edit_todo/:id"
                    component={ ()=> <EditTodo/> }> 
                </Route> */}
              </Switch>
            </div>

            <div className="footerSection bg bg-dark">
              <div className="container-fluid">
                <h2>Ahmed sohel....</h2>
              </div>
            </div>

            

        </div>
    </Router>
  );
}

export default App;



// const mapStateToProps = (state) => ({
//   //User: state.user,
// });
// const mapDispatchToProps = (dispatch) => ({
//   //
// });

// export default connect(mapStateToProps, mapDispatchToProps)(App);
